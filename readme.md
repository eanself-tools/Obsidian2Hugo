# Obsidian2Hugo

可以將 Obsidian 的筆記轉換成 Hugo 的筆記。

目前支援的轉換功能有：

- Wiki Link 轉換成 Hugo 的筆記連結
- Markdown Link 轉換成 Hugo 的筆記連結
- 將 Markdown 使用圖檔，若是指定相對路徑的圖檔，會一併複製到目標資料夾的相對路徑

## 使用方式

- `-s` 指定來源資料夾
- `-t` 指定目標資料夾
- `--ew` 指定不轉換的 Wiki Link
- `--olb` 輸出為 Hugo 的 Leaf Bundle 資料夾結構

```shell
Obsidian2Hugo.exe -s "source" -t "target" --ew "[[source\excluded]]"
```
