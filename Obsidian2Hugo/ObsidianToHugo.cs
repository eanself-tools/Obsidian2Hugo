﻿using System.Text;
using System.Text.RegularExpressions;

namespace Obsidian2Hugo;

public class ObsidianToHugo
{
    public void Run(Options options)
    {
        if (options.IsClear)
        {
            // 清空 hugoContentDir
            ClearDir(options.Target);
        }

        // 復制 obsidianVaultDir 到 hugoContentDir
        CopyObsidianVaultToHugoContentDir(options);
    }

    /// <summary>
    /// 清空資料夾內容
    /// </summary>
    /// <param name="hugoContentDir"></param>
    private static void ClearDir(string hugoContentDir)
    {
        Console.WriteLine("Clear Directories.");
        Directory.Delete(hugoContentDir, true);
        Directory.CreateDirectory(hugoContentDir);
    }

    /// <summary>
    /// 将 Obsidian Vault 的内容复制到 Hugo 的 content 目录
    /// </summary>
    /// <param name="sourceRootDir"></param>
    /// <param name="targetRootDir"></param>
    /// <param name="excludeWikiLinks"></param>
    private static void CopyObsidianVaultToHugoContentDir(Options options)
    {
        var fullFileNames = Directory.GetFiles(options.Source, "*.md", SearchOption.AllDirectories);
        Console.WriteLine("共有 {0} 個 Markdown 檔案。", fullFileNames.Length);

        // 復制檔案
        var mapper = CopyMarkdownFile(options, fullFileNames);

        // *.md 連結轉換為 Hugo 的格式
        ReplaceHugoLink(mapper, options.ExcludeWikiLinks);
    }

    private static void ReplaceHugoLink(Dictionary<string, string> mapper, IEnumerable<string> excludeWikiLinks)
    {
        foreach (var map in mapper)
        {
            var content = File.ReadAllText(map.Value, Encoding.UTF8)
                              .ReplaceMdMarks()
                              .ReplaceWikiLinks(excludeWikiLinks)
                              .ReplaceFileReference(Path.GetDirectoryName(map.Value)!, mapper)
                              .ReplaceHugoRefLinks();

            File.WriteAllText(map.Value, content, Encoding.UTF8);
        }
    }

    private static Dictionary<string, string> CopyMarkdownFile(Options options, string[] fullFileNames)
    {
        var mapper = new Dictionary<string, string>();

        foreach (var fullFileName in fullFileNames)
        {
            Console.WriteLine(Path.GetFileName(fullFileName));

            // Read the markdown file
            var content = File.ReadAllText(fullFileName, Encoding.UTF8);
            // Write the processed content to the target location
            var newFileName = fullFileName.Replace(options.Source, options.Target);

            var finalFullFileName = options.IsOutputLeafBundle == false
                                        ? CopyMarkdown(fullFileName, newFileName, content)
                                        : CopyMarkdownByLeftBundle(fullFileName, newFileName, content);

            // 記錄檔案對應
            var fileName = Path.GetFileName(fullFileName);

            mapper.Add(fileName, finalFullFileName);
        }

        return mapper;
    }

    private static string CopyMarkdownByLeftBundle(string originFileName, string newFileName, string content)
    {
        var slug = content.GetFrontMatterSlug();
        var newFileNameWithSlug = Path.Combine(Path.GetDirectoryName(newFileName)!, slug, "index.md");

        // Create the directory if it does not exist
        Directory.CreateDirectory(Path.GetDirectoryName(newFileNameWithSlug)!);

        // Copy images
        var sourceFileDirectory = Path.GetDirectoryName(originFileName)!;
        var targetFileDirectory = Path.GetDirectoryName(newFileNameWithSlug)!;
        var imagePaths = content.GetImagePaths();
        foreach (var imagePath in imagePaths)
        {
            if (imagePath.StartsWith("http", StringComparison.OrdinalIgnoreCase))
            {
                continue;
            }

            var sourceImagePath = Path.GetFullPath(Path.Combine(sourceFileDirectory, imagePath));
            var targetImagePath =
                Path.GetFullPath(Path.Combine(targetFileDirectory, "images", Path.GetFileName(sourceImagePath)));
            Directory.CreateDirectory(Path.GetDirectoryName(targetImagePath)!);
            File.Copy(sourceImagePath, targetImagePath, true);

            var newValue = $"./images/{Path.GetFileName(sourceImagePath)}";
            content = content.Replace(imagePath, newValue);
            Console.WriteLine($"{imagePath} --> {newValue}");
        }

        // Write the processed content to the target location
        File.WriteAllText(newFileNameWithSlug, content, Encoding.UTF8);
        return newFileNameWithSlug;
    }

    private static string CopyMarkdown(string originFileName, string newFileName, string content)
    {
        // Create the directory if it does not exist
        Directory.CreateDirectory(Path.GetDirectoryName(newFileName)!);

        File.WriteAllText(newFileName, content, Encoding.UTF8);

        // Copy images
        var sourceFileDirectory = Path.GetDirectoryName(originFileName)!;
        var targetFileDirectory = Path.GetDirectoryName(newFileName)!;
        var imagePaths = content.GetImagePaths();
        foreach (var imagePath in imagePaths)
        {
            if (imagePath.StartsWith("http", StringComparison.OrdinalIgnoreCase))
            {
                continue;
            }

            var sourceImagePath = Path.GetFullPath(Path.Combine(sourceFileDirectory, imagePath));
            var targetImagePath = Path.GetFullPath(Path.Combine(targetFileDirectory, imagePath));
            Directory.CreateDirectory(Path.GetDirectoryName(targetImagePath)!);
            File.Copy(sourceImagePath, targetImagePath, true);
        }

        return newFileName;
    }
}