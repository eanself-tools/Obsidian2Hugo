﻿using CommandLine;

namespace Obsidian2Hugo;

public class Options
{
    [Option('s', "source", Required = true, HelpText = "Source directory.")]
    public string Source { get; set; } = null!;

    [Option('t', "target", Required = true, HelpText = "Target directory.")]
    public string Target { get; set; } = null!;

    [Option('c', "clear", Required = false, HelpText = "Clear target directory before copy.")]
    public bool IsClear { get; set; } = false;

    [Option("ew", HelpText = "exclude wiki-links in markdown files.")]
    public IEnumerable<string> ExcludeWikiLinks { get; set; } = new List<string>();
    
    [Option("olb", HelpText = "output leaf bundle.")]
    public bool IsOutputLeafBundle { get; set; } = false;
}