﻿using System.Text.RegularExpressions;

namespace Obsidian2Hugo;

public static class HugoMarkdownConvertExtension
{
    public static string ReplaceMdMarks(this string text)
    {
        Regex mdMarksRegex = new Regex(@"==([^=\n]+)==");
        string htmlText = mdMarksRegex.Replace(text, match => $"<mark>{match.Groups[1].Value}</mark>");
        return htmlText;
    }

    public static string ReplaceWikiLinks(this string text, IEnumerable<string> excludeWikiLinks)
    {
        var wikiLinkRegex = new Regex(@"\[\[(.*?)\]\]");
        var hugoText = wikiLinkRegex.Replace(text, match =>
        {
            var excludeWikiLink = excludeWikiLinks.FirstOrDefault(x => match.Groups[0].Value.Contains(x));
            if (excludeWikiLink != null)
            {
                return match.Groups[0].Value;
            }

            string[] parts = match.Groups[1].Value.Split('|');
            string link = parts[0];
            string label = parts.Length > 1 ? parts[1] : parts[0];

            if (link.EndsWith("_index"))
            {
                link = link.Substring(0, link.Length - 6);
            }

            link = link.Split('#')
                       .Select((item, index) =>
                       {
                           if (index > 0)
                           {
                               item = item.ToLower().Replace(" ", "-");
                           }

                           return index == 0 && !item.EndsWith(".md") ? item + ".md" : item;
                       })
                       .Aggregate((a, b) => $"{a}#{b}");

            return $"[{label}]({link})";
        });

        return hugoText;
    }

    public static string ReplaceHugoRefLinks(this string input)
    {
        const string pattern = @"\(([^)]*\.md[^)]*)\)";
        Regex mdMarksRegex = new Regex(pattern);
        var result = mdMarksRegex.Replace(input, match =>
        {
            string link = match.Groups[1].Value;
            var s = $"({{{{< ref \"{link}\" >}}}})";
            return s;
        });

        return result;
    }
}