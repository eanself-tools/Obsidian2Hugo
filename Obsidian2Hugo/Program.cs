﻿// See https://aka.ms/new-console-template for more information

using CommandLine;
using Obsidian2Hugo;

Parser.Default.ParseArguments<Options>(args)
      .WithParsed(RunOptions)
      .WithNotParsed(HandleParseError);

static void RunOptions(Options opts)
{
    var obsidianToHugo = new ObsidianToHugo();
    obsidianToHugo.Run(opts);
}

static void HandleParseError(IEnumerable<Error> errs)
{
    // Insert code to handle parsing errors here
}