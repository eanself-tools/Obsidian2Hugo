﻿using System.Text.RegularExpressions;

namespace Obsidian2Hugo;

public static class MarkdownExtension
{
    public static string GetFrontMatterSlug(this string content)
    {
        var slugRegex = new Regex(@"slug: (.*)");
        var match = slugRegex.Match(content);
        if (match.Success)
        {
            var slug = match.Groups[1].Value.TrimEnd();
            return slug;
        }

        return string.Empty;
    }

    public static string ReplaceFileReference(this string text,
                                              string currentDirectory,
                                              Dictionary<string, string> refs)
    {
        const string pattern = @"\(([^)]*\.md[^)]*)\)";
        Regex mdMarksRegex = new Regex(pattern);
        var result = mdMarksRegex.Replace(text, match =>
        {
            string link = match.Groups[1].Value;

            var origin = System.Net.WebUtility.UrlDecode(Path.GetFileName(link)).Split('#');
            var mdFileName = origin[0];
            if (refs.ContainsKey(mdFileName) == false)
            {
                return match.Groups[0].Value;
            }

            var target = Path.GetRelativePath(currentDirectory, refs[mdFileName])
                             .Replace('\\', '/');

            var s = origin.Length > 1 ? $"({target}#{origin[1]})" : $"({target})";
            return s;
        });

        return result;
    }

    /// <summary>
    /// 取得所有的圖片路徑
    /// </summary>
    /// <param name="content">Markdown 內容</param>
    public static IEnumerable<string> GetImagePaths(this string content)
    {
        var imagePaths = new List<string>();
        var imageRegex = new Regex(@"!\[(.*?)\]\((.*?)\)");

        var matches = imageRegex.Matches(content);

        foreach (Match match in matches)
        {
            imagePaths.Add(match.Groups[2].Value);
        }

        return imagePaths.Distinct();
    }
}